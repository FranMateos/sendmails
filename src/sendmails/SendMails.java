/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sendmails;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import javax.mail.internet.AddressException;
/**
 *
 * @author fjmateos
 */
public class SendMails {

    /**
     * @param args the command line arguments
     */
    private static String FROM;
    private static String PASSWORD;
    private static String TO;
    private static String HOST;
    private static String PORT;
    private static boolean AUTH;
    private static String ENABLE_TLS;
    private static String REQUIRE_TLS;
    private static String MESSAGE;
    private static String SUBJECT;

    public static void main(String[] args) throws IOException {
        if(getProperties()){
            send();
        }else{
            System.out.println("Error en la carga de propiedades");
        }
    }
    
    public static boolean getProperties(){
        try (InputStream input = new FileInputStream("config.properties")) {

            Properties prop = new Properties();
            // load a properties file
            prop.load(input);
            // carga de variables
            FROM = prop.getProperty("mail.user");
            PASSWORD = prop.getProperty("mail.password");
            TO = prop.getProperty("mail.to");
            MESSAGE = prop.getProperty("mail.message");
            SUBJECT = prop.getProperty("mail.subject");
            HOST = prop.getProperty("mail.smtp.host");
            PORT = prop.getProperty("mail.smtp.port");
            AUTH = Boolean.parseBoolean(prop.getProperty("mail.smtp.auth"));
            ENABLE_TLS = prop.getProperty("mail.smtp.starttls.enable");
            REQUIRE_TLS = prop.getProperty("mail.smtp.starttls.required");
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void send() {

    Properties props = System.getProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.host", HOST);
    props.put("mail.smtp.port", PORT);
    // props.put("mail.smtp.connectiontimeout", timeout);
    props.put("mail.smtp.auth", AUTH);
    props.put("mail.smtp.starttls.enable", ENABLE_TLS);
    props.put("mail.smtp.starttls.required", REQUIRE_TLS);

    Session session = Session.getInstance(props, new javax.mail.Authenticator()
    {
      protected PasswordAuthentication getPasswordAuthentication()
      {
        return new PasswordAuthentication(FROM, PASSWORD);
      }
    });

    try {
        // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(session);
        // Set From: header field of the header.
        message.setFrom(new InternetAddress(FROM));
        // Set To: header field of the header.
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        // Set Subject: header field
        message.setSubject(SUBJECT);
        // Now set the actual message
        message.setText(MESSAGE);
        Transport.send(message);
        System.out.println("Sent message successfully....");
    } catch (MessagingException mex) {
        mex.printStackTrace();
    }
  }
}
